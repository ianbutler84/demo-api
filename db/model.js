const mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

const userSchema = new mongoose.Schema({
  username: { type: String, required: true },
  password: { type: String, required: true },
  name: { type: String, required: true },
  role: { type: String, required: true },
});

const employeeSchema = new mongoose.Schema({
  name: { type: String, required: true, unique: true },
  nickname: { type: String },
  imageUrl: { type: String },
  team: [{ type: String }],
});

const employeeDataSchema = new mongoose.Schema({
  name: { type: String, required: true },
  nickname: { type: String },
  install: { type: Number, required: true },
  kwi: { type: Number, required: true },
  wc: { type: Number, required: true },
  pr: { type: Number, required: true },
});

const reportSchema = new mongoose.Schema({
  dateStart: { type: Number, required: true },
  dateEnd: { type: Number, required: true },
  users: [{ type: employeeDataSchema, required: true }],
  title: { type: String, required: true },
  lastUpdated: Date,
  googleSheetId: String
});

const teamSchema = new mongoose.Schema({
  name: { type: String, required: true },
});

const Team = mongoose.model("Team", teamSchema);
const User = mongoose.model("User", userSchema);
const Employee = mongoose.model("Employee", employeeSchema);
const Report = mongoose.model("Report", reportSchema);

module.exports = { Employee, Report, User, Team };
