require("dotenv").config();
require("./db");
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const employeeRouter = require("./apis/employee");
const teamRouter = require("./apis/team");
const reportRouter = require("./apis/report");
const authRouter = require("./apis/auth");
const userRouter = require("./apis/user");
const app = express();
const port = process.env.PORT || 5317;

app.use(cors());
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/api/employee", employeeRouter);
app.use("/api/team", teamRouter);
app.use("/api/report", reportRouter);
app.use("/api/auth", authRouter);
app.use("/api/user", userRouter);

app.get("/api", (req, res) => {
  res.status(200).send("PING!");
});

app.listen(port, () => {
  console.log(`Example app listening at port ${port}`);
});
