const express = require("express");
const bcrypt = require("bcryptjs");
const User = require("../db/model").User;
const authenticateJWTAdminOnly = require("./utils").authenticateJWTAdminOnly;
const authenticateJWT = require("./utils").authenticateJWT;

const router = express.Router();

// /api/user
router.get("/", authenticateJWTAdminOnly, (req, res) => {
  User.find((err, arr) => {
    if (err) {
      return res.status(400).send(err);
    }
    return res.status(200).send(arr);
  });
});

router.post("/admin", authenticateJWTAdminOnly, (req, res) => {
  const { name, username, password } = req.body;

  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(password, salt);

  User.create(
    {
      name,
      username,
      password: hash,
      role: "admin",
    },
    (err, user) => {
      if (err) {
        return res.status(400).send(err);
      }
      res
        .status(200)
        .send({ name: user.name, username: user.username, role: user.role });
    }
  );
});

router.post("/", authenticateJWTAdminOnly, async (req, res) => {
  const { name, username, password, role } = req.body;

  const salt = await bcrypt.genSalt();
  const hash = await bcrypt.hash(password, salt);

  User.create(
    {
      name,
      username,
      password: hash,
      role,
    },
    (err, user) => {
      if (err) {
        return res.status(400).send(err);
      }
      res
        .status(200)
        .send({ name: user.name, username: user.username, role: user.role });
    }
  );
});

router.put("/:id", authenticateJWTAdminOnly, (req, res) => {
  const { name, role, username } = req.body;
  const { id } = req.params;

  User.findByIdAndUpdate(id, { name, role, username }, (err, user) => {
    if (err) {
      return res.status(400).send(err);
    }

    res.status(200).send(user);
  });
});

router.put("/:id/passwordreset", authenticateJWT, (req, res) => {
  const { password, previousPassword } = req.body;
  const { id } = req.params;

  User.findById(id, (err, user) => {
    if (err) {
      return res.status(400).send(err);
    }

    if (!user) {
      return res.status(400).send("No user found");
    }

    bcrypt.compare(previousPassword, user.password, async (err, result) => {
      if (err) {
        return res.status(400).send(err);
      }

      if (!result) {
        return res.status(401).send("Password is wrong");
      }

      const salt = await bcrypt.genSalt();
      const hash = await bcrypt.hash(password, salt);

      User.findByIdAndUpdate(id, { password: hash }, (err, updatedUser) => {
        if (err) {
          return res.status(400).send(err);
        }

        if (!updatedUser) {
          return res.status(400).send("no user found...");
        }

        res.status(200).send(user);
      });
    });
  });
});

// router.put("/:id/auth", authenticateJWTAdminOnly, (req, res) => {
//   const { username, password } = req.body;
//   const { id } = req.params;

//   User.findByIdAndUpdate(id, { username, password }, (err, user) => {
//     if (err) {
//       return res.status(400).send(err);
//     }

//     res.status(200).send(user);
//   });
// });

router.delete("/:id", authenticateJWTAdminOnly, (req, res) => {
  const { id } = req.params;

  User.findByIdAndDelete(id, (err, user) => {
    if (err) {
      return res.status(400).send(err);
    }
    if (!user) {
      return res.status(400).send("user not found");
    }
    if (user.role === "admin") {
      return res.status(403).send("Cannot delete admin");
    }

    res.status(200).send(user);
  });
});

module.exports = router;
