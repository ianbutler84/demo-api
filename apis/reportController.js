const { Report } = require("../db/model");
const { getSpreadsheetData, getSpreadsheetDataSingle } = require("../google-sheets");

function singleUpdateController(title, googleSheetId) {
  try {
    getSpreadsheetDataSingle(title, googleSheetId)
      .then((data) => {
        let promiseArr = [];

        data.forEach((report) => {
          promiseArr.push(updateReport(report));
        });

        return Promise.all(promiseArr)
          .then(() => {
            return true;
          })
          .catch((err) => {
            return err;
          });
      })
      .catch((err) => {
        return err;
      });
  } catch(e) {
    console.log('error attempting to get spreadsheet data', e);
  }
}

function massUpdateController() {
  try {
    return getSpreadsheetData()
      .then(() => {
        console.log("getSpreadsheetData was called");
      })
      .catch((err) => {
        return err;
      });
  } catch(e) {
    console.log('error attempting to get spreadsheet data', e);
  }
}

const updateReport = (newReport) => {
  return Report.findOneAndUpdate(
    { title: newReport.title },
    { lastUpdated: newReport.lastUpdated, googleSheetId: newReport.googleSheetId },
    newReport,
    (err, report) => {
      if (err) {
        throw err;
      }
      if (!report) {
        return createReport(newReport);
      }
      return report;
    }
  );
}

function createReport(newReport) {
  return Report.create(newReport, (err, report) => {
    if (err) {
      throw err;
    }
    return report;
  });
}

module.exports = {
  massUpdateController,
  singleUpdateController,
  updateReport
};
