const express = require("express");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const User = require("../db/model").User;

const router = express.Router();
const accessTokenSecret = process.env.ACCESS_TOKEN_SECRET;

router.post("/login", (req, res) => {
  const { username, password } = req.body;

  User.findOne({ username }, (err, user) => {
    if (err) {
      return res.status(400).send(err);
    }
    if (!user) {
      return res.status(401).send("No user found with that username");
    }

    bcrypt.compare(password, user.password, (err, result) => {
      if (err) {
        return res.status(400).send(err);
      }

      if (!result) {
        return res.status(401).send("Password is wrong");
      }

      const token = jwt.sign(
        {
          username: user.username,
          role: user.role,
          id: user._id,
          exp: Math.floor(Date.now() / 1000) + 60 * 180,
        },
        accessTokenSecret
      );

      return res.status(200).send({ token });
    });
  });
});

module.exports = router;
