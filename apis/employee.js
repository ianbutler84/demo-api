const express = require("express");
const Employee = require("../db/model").Employee;
const authenticateJWT = require("./utils").authenticateJWT;
const authenticateJWTAdminOnly = require("./utils").authenticateJWTAdminOnly;

const router = express.Router();

router.use((req, res, next) => {
  authenticateJWT(req, res, next);
});

// /api/employee
router.get("/", (req, res) => {
  Employee.find((err, arr) => {
    if (err) {
      return res.status(400).send(err);
    }
    return res.status(200).send(arr);
  });
});

router.post("/", authenticateJWTAdminOnly, (req, res) => {
  const { name, nickname, imageUrl, team } = req.body;

  Employee.create(
    {
      name,
      imageUrl,
      team,
      nickname,
    },
    (err, employee) => {
      if (err) {
        return res.status(400).send(err);
      }
      res.status(200).send(employee);
    }
  );
});

router.put("/:id", authenticateJWTAdminOnly, (req, res) => {
  const { name, imageUrl, team, nickname } = req.body;
  const { id } = req.params;

  Employee.findByIdAndUpdate(
    id,
    { name, imageUrl, team, nickname },
    (err, employee) => {
      if (err) {
        return res.status(400).send(err);
      }

      res.status(200).send(employee);
    }
  );
});

router.delete("/:id", authenticateJWTAdminOnly, (req, res) => {
  const { id } = req.params;

  Employee.findByIdAndDelete(id, (err, employee) => {
    if (err) {
      return res.status(400).send(err);
    }
    if (!employee) {
      return res.status(400).send("employee not found");
    }
    res.status(200).send(employee);
  });
});

module.exports = router;
