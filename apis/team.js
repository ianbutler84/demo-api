const express = require("express");
const Team = require("../db/model").Team;
const authenticateJWT = require("./utils").authenticateJWT;
const authenticateJWTAdminOnly = require("./utils").authenticateJWTAdminOnly;

const router = express.Router();

router.use((req, res, next) => {
  authenticateJWT(req, res, next);
});

// /api/team
router.get("/", (req, res) => {
  Team.find((err, arr) => {
    if (err) {
      return res.status(400).send(err);
    }
    return res.status(200).send(arr);
  });
});

router.post("/", authenticateJWTAdminOnly, (req, res) => {
  const { name } = req.body;

  if (name === "admin" || name === "viewer") {
    return res.status(400).send("You can't name a team admin or viewer");
  }

  Team.create(
    {
      name,
    },
    (err, team) => {
      if (err) {
        return res.status(400).send(err);
      }
      res.status(200).send(team);
    }
  );
});

router.put("/:id", authenticateJWTAdminOnly, (req, res) => {
  const { name } = req.body;
  const { id } = req.params;

  Team.findByIdAndUpdate(id, { name }, (err, team) => {
    if (err) {
      return res.status(400).send(err);
    }

    res.status(200).send(team);
  });
});

router.delete("/:id", authenticateJWTAdminOnly, (req, res) => {
  const { id } = req.params;

  Team.findByIdAndDelete(id, (err, team) => {
    if (err) {
      return res.status(400).send(err);
    }
    if (!team) {
      return res.status(400).send("team not found");
    }
    res.status(200).send(team);
  });
});

module.exports = router;
