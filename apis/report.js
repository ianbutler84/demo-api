const express = require("express");
const Report = require("../db/model").Report;
const Employee = require("../db/model").Employee;
const authenticateJWT = require("./utils").authenticateJWT;
const authenticateJWTAdminOnly = require("./utils").authenticateJWTAdminOnly;
const { massUpdateController, singleUpdateController } = require("./reportController");
const router = express.Router();

router.use((req, res, next) => {
  authenticateJWT(req, res, next);
});

// get all reports
router.get("/", (req, res) => {
  Report.find((err, reportArr) => {
    if (err) {
      return res.status(400).send(err);
    }

    Employee.find((err, employeeArr) => {
      if (err) {
        return res.status(400).send(err);
      }

      const employeeRefObj = {};

      employeeArr.forEach((emp) => {
        employeeRefObj[emp.name.trim()] = {
          image: emp.imageUrl || "",
          team: emp.team || [],
          nickname: emp.nickname || "",
        };
      });

      const returnArr = reportArr
        .sort((a, b) => a.dateStart - b.dateStart)
        .map((report) => {
          const newUsersArr = report.users.map((user) => {
            const trimmedName = user.name.trim();
            return {
              _id: user._id,
              name: trimmedName,
              install: user.install,
              pr: user.pr,
              kwi: user.kwi,
              wc: user.wc,
              imageUrl: employeeRefObj[trimmedName]
                ? employeeRefObj[trimmedName].image
                : "",
              nickname: employeeRefObj[trimmedName]
                ? employeeRefObj[trimmedName].nickname
                : "",
              team: employeeRefObj[trimmedName]
                ? employeeRefObj[trimmedName].team
                : [],
            };
          });

          return {
            title: report.title,
            _id: report._id,
            dateStart: report.dateStart,
            dateEnd: report.dateEnd,
            users: newUsersArr,
            lastUpdated: report.lastUpdated,
            googleSheetId: report.googleSheetId
          };
        });

      return res.status(200).send(returnArr);
    });
  });
});

// create report
router.post("/", authenticateJWTAdminOnly, (req, res) => {
  const { dateStart, dateEnd, users, title } = req.body;

  Report.create(
    {
      dateStart,
      dateEnd,
      users,
      title,
    },
    (err, report) => {
      if (err) {
        return res.status(400).send(err);
      }
      res.status(200).send(report);
    }
  );
});

// create custom report
router.post("/custom", (req, res) => {
  const { dateStart, dateEnd } = req.body;

  Report.find((err, reportArr) => {
    if (err) {
      return res.status(400).send(err);
    }

    Employee.find((err, employeeArr) => {
      if (err) {
        return res.status(400).send(err);
      }

      const validReports = reportArr.filter(
        (report) => dateStart < report.dateEnd && dateEnd > report.dateStart
      );

      const compiledReport = {
        title: `Week of ${new Date(
          dateStart * 1000
        ).toLocaleDateString()} through week of ${new Date(
          dateEnd * 1000
        ).toLocaleDateString()}`,
        users: [],
        dateStart,
        dateEnd,
      };

      const usersObj = {};

      const employeeRefObj = {};

      employeeArr.forEach((emp) => {
        employeeRefObj[emp.name.trim()] = {
          image: emp.imageUrl || "",
          team: emp.team || [],
          nickname: emp.nickname || "",
        };
      });

      for (let i = 0; i < validReports.length; i++) {
        const rep = validReports[i];
        for (let j = 0; j < rep.users.length; j++) {
          const user = rep.users[j];
          const trimmedName = user.name.trim();
          if (usersObj[trimmedName]) {
            usersObj[trimmedName].install += user.install;
            usersObj[trimmedName].kwi += user.kwi;
            usersObj[trimmedName].pr += user.pr;
            usersObj[trimmedName].wc += user.wc;
            usersObj[trimmedName].imageUrl = employeeRefObj[trimmedName]
              ? employeeRefObj[trimmedName].image
              : "";
            usersObj[trimmedName].nickname = employeeRefObj[trimmedName]
              ? employeeRefObj[trimmedName].nickname
              : "";
            usersObj[trimmedName].team = employeeRefObj[trimmedName]
              ? employeeRefObj[trimmedName].team
              : [];
          } else {
            usersObj[trimmedName] = {
              install: user.install,
              kwi: user.kwi,
              pr: user.pr,
              wc: user.wc,
              imageUrl: employeeRefObj[trimmedName]
                ? employeeRefObj[trimmedName].image
                : "",
              nickname: employeeRefObj[trimmedName]
                ? employeeRefObj[trimmedName].nickname
                : "",
              team: employeeRefObj[trimmedName]
                ? employeeRefObj[trimmedName].team
                : [],
            };
          }
        }
      }

      for (let person in usersObj) {
        compiledReport.users.push({
          name: person,
          install: usersObj[person].install,
          wc: usersObj[person].wc,
          pr: usersObj[person].pr,
          kwi: usersObj[person].kwi,
          imageUrl: usersObj[person].imageUrl,
          nickname: usersObj[person].nickname,
          team: usersObj[person].team,
        });
      }

      res.status(200).send(compiledReport);
    });
  });
});

// edit single report
router.put("/:id", authenticateJWTAdminOnly, (req, res) => {
  const { dateStart, dateEnd, title } = req.body;
  const { id } = req.params;

  Report.findByIdAndUpdate(id, { dateStart, dateEnd, title }, (err, report) => {
    if (err) {
      return res.status(400).send(err);
    }

    res.status(200).send(report);
  });
});

// delete report
router.delete("/:id", authenticateJWTAdminOnly, (req, res) => {
  const { id } = req.params;

  Report.findByIdAndDelete(id, (err, report) => {
    if (err) {
      return res.status(400).send(err);
    }
    if (!report) {
      return res.status(400).send("report not found");
    }
    res.status(200).send(report);
  });
});

// populate reports from google sheet
router.post("/update", authenticateJWTAdminOnly, (req, res) => {
  massUpdateController()
  return res.status(200).send("Success");
});

router.post("/update/single", authenticateJWTAdminOnly, (req, res) => {
  const { title, googleSheetId } = req.body;
  singleUpdateController(title, googleSheetId)
    .then(() => {
      return res.status(200).send("Success");
    })
    .catch((err) => {
      return res.status(400).send(err);
    });
});

module.exports = router;
