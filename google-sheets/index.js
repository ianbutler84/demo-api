const { GoogleSpreadsheet } = require("google-spreadsheet");
const moment = require("moment");
const { Report } = require("../db/model");

function createReport(newReport) {
  return Report.create(newReport, (err, report) => {
    if (err) {
      throw err;
    }
    return report;
  });
}

const updateReport = (newReport) => {
  return Report.findOneAndUpdate(
    { title: newReport.title },
    { lastUpdated: newReport.lastUpdated, googleSheetId: newReport.googleSheetId, users: newReport.users },
    newReport,
    (err, report) => {
      if (err) {
        throw err;
      }
      if (!report) {
        return createReport(newReport);
      }
      return report;
    }
  );
}

function getSpreadsheetData() {
  // Initialize the sheet - doc ID is the long id in the sheets URL
  const doc = new GoogleSpreadsheet(process.env.SCORECARD_SPREADSHEET_ID);

  return doc
    .useServiceAccountAuth({
      client_email: process.env.GOOGLE_SERVICE_ACCOUNT_EMAIL,
      private_key: process.env.GOOGLE_SERVICE_ACCOUNT_PRIVATE_KEY.replace(
        /\\n/g,
        "\n"
      ),
    })
    .then(async () => {
      const promiseReportData = [];

      await doc.loadInfo(); // loads sheets

      const sheetCount = doc.sheetCount;

      for (let i = 0; i < sheetCount; i++) {
        promiseReportData.push(new Promise(((resolve) => {
          setTimeout(async () => {   
            updateSheet(doc, i)         
            resolve();
          }, Math.floor(i / 10) * 65000);
        })));
      }

      return Promise.all(promiseReportData);
    })
    .catch((err) => {
      console.log("ERROR Authenticating with google sheets", err);
      throw err;
    });
}

const updateSheet = async (doc, i) => {
  const sheet = doc.sheetsByIndex[i];

  const userData = [];

  const rows = await sheet.getRows();
  const rowLength = rows.length;

  let j = 0;
  while (j < rowLength) {
    let row = rows[j];

    if (!row.Name || row.Name === "Total") {
      j++;
      continue;
    }

    const userObj = {
      name: row.Name,
      install: parseFloat(row.Install) || 0,
      kwi: parseFloat(row.KWI) || 0,
      pr: parseFloat(row.PR) || 0,
      wc: parseFloat(row.WC) || 0,
    };

    userData.push(userObj);
    j++;
  }

  await sheet.loadCells();

  const dateStart = await sheet.getCellByA1("K2").formattedValue;
  const dateEnd = await sheet.getCellByA1("L2").formattedValue;

  const jsonObj = {
    title: `Week ${i} ${dateStart}-${dateEnd}`,
    lastUpdated: Date.now(),
    googleSheetId: sheet.sheetId,
    dateStart: moment.utc(dateStart).unix(),
    dateEnd: moment.utc(dateEnd).unix(),
    users: userData,
  };

  updateReport(jsonObj);
}

function getSpreadsheetDataSingle(title, id) {
  // Initialize the sheet - doc ID is the long id in the sheets URL
  console.log("MONITOR: pre new GoogleSpreadsheet")
  const doc = new GoogleSpreadsheet(process.env.SCORECARD_SPREADSHEET_ID);
  console.log("MONITOR: post new GoogleSpreadsheet")
  
  console.log("MONITOR: pre useServiceAccountAuth")
  return doc
    .useServiceAccountAuth({
      client_email: process.env.GOOGLE_SERVICE_ACCOUNT_EMAIL,
      private_key: process.env.GOOGLE_SERVICE_ACCOUNT_PRIVATE_KEY.replace(
        /\\n/g,
        "\n"
      ),
    })
    .then(async () => {
      console.log("MONITOR: SUCCESSFUL useServiceAccountAuth")

      const allReportData = [];

      console.log("MONITOR: pre doc.loadInfo()")
      await doc.loadInfo(); // loads sheets
      
      const sheet = doc.sheetsById[id];

      const userData = [];

      console.log(`MONITOR: pre row id:${id}  sheet.getRows()`)
      const rows = await sheet.getRows();
      console.log(`MONITOR: success row id:${id}  sheet.getRows()`)
      const rowLength = rows.length;

      let j = 0;
      while (j < rowLength) {
        let row = rows[j];

        if (!row.Name || row.Name === "Total") {
          j++;
          continue;
        }

        const userObj = {
          name: row.Name,
          install: parseFloat(row.Install) || 0,
          kwi: parseFloat(row.KWI) || 0,
          pr: parseFloat(row.PR) || 0,
          wc: parseFloat(row.WC) || 0,
        };

        userData.push(userObj);
        j++;
      }

      console.log(`MONITOR: pre id:${id}  sheet.loadCells()`)
      await sheet.loadCells();
      console.log(`MONITOR: success id:${id}  sheet.loadCells()`)

      console.log(`MONITOR: pre row id:${id}  sheet.getCellByA1(K2)`)
      const dateStart = await sheet.getCellByA1("K2").formattedValue;
      console.log(`MONITOR: success row id:${id}  sheet.getCellByA1(K2)`)

      console.log(`MONITOR: pre row id:${id}  sheet.getCellByA1(L2)`)
      const dateEnd = await sheet.getCellByA1("L2").formattedValue;
      console.log(`MONITOR: success row id:${id}  sheet.getCellByA1(L2)`)

      const jsonObj = {
        title,
        lastUpdated: Date.now(),
        dateStart: moment.utc(dateStart).unix(),
        dateEnd: moment.utc(dateEnd).unix(),
        users: userData,
        googleSheetId: id
      };

      allReportData.push(jsonObj);

      return allReportData;
    })
    .catch((err) => {
      console.log("ERROR Authenticating with google sheets", err);
      throw err;
    });
}

module.exports = { getSpreadsheetData, getSpreadsheetDataSingle };
