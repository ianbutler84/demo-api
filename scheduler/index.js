const { ToadScheduler, SimpleIntervalJob, Task } = require("toad-scheduler");
const { massUpdateController } = require("../apis/reportController");

function scheduler(app) {
  const scheduler = new ToadScheduler();

  const task = new Task("simple task", () => {
    massUpdateController();
    console.log("Mass google sheets update job ran!");
  });

  const job = new SimpleIntervalJob({ minutes: 10 }, task);

  scheduler.addSimpleIntervalJob(job);

  app.on("close", () => {
    scheduler.stop();
  });
}

module.exports = { scheduler };
